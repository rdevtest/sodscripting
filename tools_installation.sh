#!/usr/bin/env bash

apt-get update
apt-get install unzip curl openjdk-11-jre zip -y
curl -s "https://get.sdkman.io" | bash
GRADLE_VERSION="7.5.1"
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install gradle $GRADLE_VERSION
ln -s $HOME/.sdkman/candidates/gradle/current/bin/gradle /usr/bin/gradle

# Install Android SDK 
# https://developer.android.com/studio/command-line/sdkmanager

SDK_URL="https://dl.google.com/android/repository/commandlinetools-linux-8512546_latest.zip"
mkdir $ANDROID_HOME
curl -L $SDK_URL -o sdk.zip
unzip sdk.zip -d $ANDROID_HOME
rm sdk.zip
cd $ANDROID_HOME/cmdline-tools
mkdir latest
mv NOTICE.txt bin/ lib/ source.properties latest