FROM ubuntu:jammy

ENV ANDROID_HOME="/usr/local/android-sdk" \
    ANDROID_VERSION=32 \
    ANDROID_BUILD_TOOLS_VERSION=32.0.0

WORKDIR /home/gradle
COPY tools_installation.sh ./
RUN chmod +x tools_installation.sh
RUN ./tools_installation.sh

RUN $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager --update
RUN echo y | $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
     "platforms;android-${ANDROID_VERSION}" \
     "platform-tools"
CMD [ "gradle", "--version" ]